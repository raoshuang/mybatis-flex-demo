# MyBatis-Flex 的配置选项

## 一、application.yml 的配置

```yml
mybatis-flex:  
    #......  
    datasource:  
        #......  
    configuration:  
        #......  
    global-config:  
        #......  
    admin-config:  
        #......  
```

## 二、自定义 MyBatis Configuration

```java
/**
 * 为 {@link FlexConfiguration} 做自定义的配置支持。
 */
@FunctionalInterface
public interface ConfigurationCustomizer {

    /**
     * 自定义配置 {@link FlexConfiguration}。
     *
     * @param configuration MyBatis Flex Configuration
     */
    void customize(FlexConfiguration configuration);

}
```

## 三、自定义 SqlSessionFactoryBean

```java
/**
 * 为 FlexSqlSessionFactoryBean 做自定义的配置支持。
 *
 * @see com.mybatisflex.spring.FlexSqlSessionFactoryBean
 */
@FunctionalInterface
public interface SqlSessionFactoryBeanCustomizer {

    /**
     * 自定义 {@link SqlSessionFactoryBean}。
     *
     * @param factoryBean FlexSqlSessionFactoryBean
     */
    void customize(SqlSessionFactoryBean factoryBean);

}
```

## 四、MyBatisFlexCustomizer

```java
/**
 * <p>MyBatis-Flex 配置。
 *
 * <p>一般可以用于去初始化：
 *
 * <ul>
 *      <li>FlexGlobalConfig 的全局配置
 *      <li>自定义主键生成器
 *      <li>多租户配置
 *      <li>动态表名配置
 *      <li>逻辑删除处理器配置
 *      <li>自定义脱敏规则
 *      <li>SQL 审计配置
 *      <li>SQL 打印配置
 *      <li>数据源解密器配置
 *      <li>自定义数据方言配置
 *      <li>...
 * </ul>
 */
public interface MyBatisFlexCustomizer {

    /**
     * 自定义 MyBatis-Flex 配置。
     *
     * @param globalConfig 全局配置
     */
    void customize(FlexGlobalConfig globalConfig);

}
```

## 五、配置选项的加载顺序

- MyBatis Flex 配置！

- MyBatis Configuration 配置！

- SqlSessionFactoryBean 配置！


