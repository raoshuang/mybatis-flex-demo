# 基于 SpringBoot 的快速开始

## 一、快速搭建上手项目

- 支持多种框架适配

- 按照官网的步骤走

## 二、自动生成类的简单说明

- 生成的 AccountTableDef 在哪

- 怎样才能生成 AccountTableDef 类

- 如何使用 AccountTableDef 类

## 三、其他错误的注意事项

- 换用 Druid 数据源

- 使用 SpringBoot 3.0 注意事项


