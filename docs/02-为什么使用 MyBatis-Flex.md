# 为什么使用 MyBatis-Flex

![QQ截图20230705083449](D:\Documents\MyBatis-Flex\评论\2023-07-05\QQ截图20230705083449.png)

## 一、与其他框架的对比

- 功能：全面且免费

- 性能：轻量实现

- 依赖：依赖少

- 扩展：多扩展

- .......

## 二、解决了什么痛点问题

- 代码可读性：MyBatis XML、MyBatis-Plus Java+XML、MyBatis-Flex java

- 效率：一层层的拦截器

- 错误定位：依赖少、传递的BUG少、错误定位迅速

- 功能：功能丰富，打包体积小

- ......

## 三、选择 MyBatis-Flex 的优势

- 更多的功能，更好的效率

- 原生项目 Naive 没有 Spring 框架

- 不想使用 XML 文件，要干掉 XML


