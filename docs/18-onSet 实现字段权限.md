# onSet 实现字段权限

```java
public class AccountPermissionListener implements SetListener {

    @Override
    public Object onSet(Object entity, String property, Object value) {
        // 只有 张三 用户才能读出 密码 信息
        if (isPassword(property)) {
            Account account = (Account) entity;

            boolean hasPermission = hasPermission(account);

            if (hasPermission) {
                return value;
            }

            return null;
        }

        // 别忘记返回其他属性的值！！！
        return value;
    }

    public boolean isPassword(String property) {
        return "password".equals(property);
    }

    public boolean hasPermission(Account account) {
        return "张三".equals(account.getUserName());
    }

}
```
