# onInsert、onUpdate 的使用

## 一、onInsert 的使用

- 实现 `com.mybatisflex.annotation.InsertListener` 接口

- 继承 `com.mybatisflex.annotation.AbstractInsertListener` 抽象类

## 二、onUpdate 的使用

* 实现 `com.mybatisflex.annotation.UpdateListener` 接口

* 继承 `com.mybatisflex.annotation.AbstractUpdateListener` 抽象类

## 三、特别说明

- 实现接口都是通用的监听器

- 继承抽象类都是特定的监听器


