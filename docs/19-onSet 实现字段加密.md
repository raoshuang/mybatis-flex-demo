# onSet 实现字段加密

```java
public class AccountCryptListener implements SetListener {

    @Override
    public Object onSet(Object entity, String property, Object value) {
        if (isUserName(property)) {
            Account account = (Account) entity;

            String userName = account.getUserName();

            if (userName != null) {
                userName = userName.codePoints()
                        .mapToObj(code -> "\\u" + code)
                        .reduce(String::concat)
                        .orElse("");
                return userName;
            }

            return null;
        }

        if (isPassword(property)) {
            return "解密：" + value;
        }

        // 别忘记返回其他属性的值！！！
        return value;
    }

    public boolean isUserName(String property) {
        return "userName".equals(property);
    }

    public boolean isPassword(String property) {
        return "password".equals(property);
    }

}
```
