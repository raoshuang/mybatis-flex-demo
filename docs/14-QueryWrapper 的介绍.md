# QueryWrapper 的介绍

## 一、QueryWrapper 的简单使用

DQL 的构建

```sql
select ...
from ...
left join ... on ...
where ... 
group by ...
having ...
order by ...
union ...
limit ...
```

## 二、动态条件的构建

```java
String userName = "张三";

boolean flag = userName != null && !userName.trim().isEmpty();

queryWrapper.where(flag ? ACCOUNT.USER_NAME.eq(userName) : QueryMethods.noCondition());
queryWrapper.where(ACCOUNT.USER_NAME.eq(userName).when(flag));
queryWrapper.where(ACCOUNT.USER_NAME.eq(userName).when(If::hasText));
queryWrapper.where(ACCOUNT.USER_NAME.eq(userName, If::hasText));
```

## 三、SQL 函数的使用

`com.mybatisflex.core.query.QueryMethods`

## 四、lambda 扩展

```java
QueryWrapper queryWrapper = QueryWrapper.create()
        .select(Account::getId, Account::getAge)
        .from(Account.class)
        .where(Account::getId).ge(3);
```

##  五、字符串扩展

```java
QueryWrapper queryWrapper = QueryWrapper.create()
        .select("id", "user_name", "age")
        .from("tb_account")
        .where("id = ?", 1);
```


