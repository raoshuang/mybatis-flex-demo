# MyBatis-Flex 简介

## 一、为什么要开发这样一个框架

> 为什么要开发这么一个框架，之前公司本来计划是使用 plus，经过调研后发现问题较多，比如对 mybatis 入侵较深，其基于 sql parse 进行增强的方式性能很差，且 sql builder 增强的灵活和自主性远高于 sql parse。

> 另外 plus 强依赖 spring，我们公司主要是做信创业务（软件国产化），目前国产框架陈出不穷，对国产化框架的兼容必不可少…

- MyBatis-Plus ------ Interceptor ------ SqlParser

- MyBatis-Flex ------ SqlProvider ------ StringBuilder

## 二、MyBatis-Flex 的诞生

**v1.0.0 20230312**：正式版发布

**v1.2.1 20230506**：开始参与开发的版本

**v1.2.2 20230510**：参与贡献的版本

## 三、MyBatis-Flex 的优点

- 更轻量：MyBatis-Flex 除了 MyBatis 本身，再无任何第三方依赖，因此会带来更高的自主性、把控性和稳定性。在任何一个系统中，依赖越多，稳定性越差。

- 更灵活：MyBatis-Flex 提供了非常灵活的 QueryWrapper，支持关联查询、多表查询、多主键、逻辑删除、乐观锁更新、数据填充、数据脱敏、等等

- 更高的性能：MyBatis-Flex 通过独特的架构，没有任何 MyBatis 拦截器、在 SQL 执行的过程中，没有任何的 SQL Parse，因此会带来指数级的性能增长。

## 四、今后的开发计划

> 接下来还有一个计划 mybatis flex admin，类似 spring admin，但是更多的功能，或者说重点是 sql 审计



**Gitee 仓库：https://gitee.com/mybatis-flex/mybatis-flex-admin**
