# 映射查询

- **`selectOneByQueryAs(queryWrapper, asType)`**: 根据查询条件来查询 1 条数据。
* **`selectListByQueryAs(queryWrapper, asType)`**: 根据查询条件查询数据列表，要求返回的数据为 asType。这种场景一般用在 left join 时，有多出了实体类本身的字段内容，可以转换为 dto、vo 等场景。
* **`selectObjectByQueryAs(queryWrapper, asType)`**: 查询第一列返回的数据。
* **`selectObjectListByQueryAs(queryWrapper, asType)`**: 查询第一列返回的数据集合。


