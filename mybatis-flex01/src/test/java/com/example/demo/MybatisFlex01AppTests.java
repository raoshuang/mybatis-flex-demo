package com.example.demo;

import com.example.demo.entity.Account;
import com.example.demo.mapper.AccountMapper;
import com.mybatisflex.core.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static com.example.demo.entity.table.AccountTableDef.*;

/**
 * mybatis-flex 测试
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 20:58
 **/
@SpringBootTest
class MybatisFlex01AppTests {

    @Autowired
    private AccountMapper accountMapper;

    @Test
    void contextLoads() {
        /*QueryWrapper queryWrapper = QueryWrapper.create()
                .select()
                .where(ACCOUNT.AGE.eq(18));
        Account account = accountMapper.selectOneByQuery(queryWrapper);
        System.out.println(account);*/
        accountMapper.selectAll().forEach(System.out::println);
    }

}
