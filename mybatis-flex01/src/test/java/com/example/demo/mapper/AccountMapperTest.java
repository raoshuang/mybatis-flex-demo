package com.example.demo.mapper;

import com.example.demo.entity.Account;
import com.example.demo.vo.AccountVO;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.row.Row;
import com.mybatisflex.core.update.UpdateWrapper;
import com.mybatisflex.core.util.UpdateEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static com.example.demo.entity.table.AccountTableDef.ACCOUNT;
import static com.mybatisflex.core.query.QueryMethods.*;

/**
 * mybatis-flex 基础api测试
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 21:39
 */
@SpringBootTest
public class AccountMapperTest {

    @Autowired
    private AccountMapper accountMapper;

    /*** 插入测试-不带主键插入 */
    @Test
    void testInsert() {
        // gender 0-女/1-男/3-未知（默认3)
        Account account = Account.builder().build();
        account.setUserName("tester1");
        // gender -> null
        account.setAge(18);
        account.setBirthday(new Date());

        // 不带主键插入
        int rows;
        // 不忽略null字段
        rows = accountMapper.insert(account);
        System.out.println("AccountId:" + account.getId());
        Assertions.assertEquals(1, rows);

        // 忽略null字段
        rows = accountMapper.insertSelective(account);
        System.out.println("AccountId:" + account.getId());
        Assertions.assertEquals(1, rows);

        // 手动设置是否忽略null字段
        rows = accountMapper.insert(account, true);
        System.out.println("AccountId:" + account.getId());
        Assertions.assertEquals(1, rows);
    }

    /*** 插入测试-带主键 */
    @Test
    void testInsertWithPk() {
        // gender 0-女/1-男/3-未知（默认3)
        Account account = Account.builder().id(6L).build();
        account.setUserName("tester2");
        // gender -> null
        account.setAge(18);
        account.setBirthday(new Date());

        int rows;
        // 不忽略null字段
        rows = accountMapper.insertWithPk(account);
        Assertions.assertEquals(1, rows);

        account.setId(7L);
        // 忽略null字段
        rows = accountMapper.insertSelectiveWithPk(account);
        Assertions.assertEquals(1, rows);

        account.setId(8L);
        // 手动设置是否忽略null字段
        rows = accountMapper.insertWithPk(account, true);
        Assertions.assertEquals(1, rows);
    }

    /*** 测试插入或者更新 */
    @Test
    void testInsertOrUpdate() {
        // gender 0-女/1-男/3-未知（默认3)
        Account account = Account.builder().id(8L).build();
        account.setUserName("tester3");
        account.setAge(20);

        int rows;
        // 不忽略null字段
        rows = accountMapper.insertOrUpdate(account);
        Assertions.assertEquals(1, rows);

        // 忽略null字段
        account.setId(null);
        rows = accountMapper.insertOrUpdateSelective(account);
        Assertions.assertEquals(1, rows);

        // 手动设置是否忽略null字段
        System.out.println(account.getId());
        rows = accountMapper.insertOrUpdate(account, true);
        Assertions.assertEquals(1, rows);
    }

    /*** 测试批量插入 */
    @Test
    void testInsertBatch() {
        // 批量更新
        ArrayList<Account> arrayList = new ArrayList<>();

        // insert into tb_account (user_name, age) values ('batch1', 18) ...
        arrayList.add(Account.builder()
                .userName("batch1")
                .age(18)
                .build());

        arrayList.add(Account.builder()
                .userName("batch2")
                .age(19)
                .build());

        arrayList.add(Account.builder()
                .userName("batch3")
                .age(20)
                .birthday(new Date())
                .build());

        // 不忽略 null 值 Db
        int rows = accountMapper.insertBatch(arrayList);
        Assertions.assertEquals(3, rows);
    }

    /*** 测试条件删除 */
    @Test
    void testDelete() {
        int rows = 0;
        rows += accountMapper.deleteById(3);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", 4);
        rows += accountMapper.deleteByMap(hashMap);
        rows += accountMapper.deleteByCondition(ACCOUNT.ID.eq(5));
        rows += accountMapper.deleteByQuery(QueryWrapper.create().where(ACCOUNT.ID.eq(6)));
        Assertions.assertEquals(4, rows);
    }

    /*** 批量删除 */
    @Test
    void testDeleteBatch() {
        // 批量删除 - 依据主键
        int rows = accountMapper.deleteBatchByIds(Arrays.asList(7, 8, 9));
        Assertions.assertEquals(3, rows);
    }

    /*** 全表数据删除禁止-执行会抛异常 */
    @Test
    void testFullDelete() {
        // 全表删除
        Assertions.assertThrows(Exception.class,
                () -> accountMapper.deleteByQuery(QueryWrapper.create()));
    }

    /*** 更新测试-根据 ID 更新 */
    @Test
    void testUpdate() {
        Account account = Account.builder().build();
        account.setId(1L);
        account.setAge(100);
        // 默认排除null字段
        int rows = accountMapper.update(account);
        Assertions.assertEquals(1, rows);
        account.setUserName("张三");
        // 指定null也更新
        rows = accountMapper.update(account, false);
        Assertions.assertEquals(1, rows);
    }

    /*** 根据条件更新 */
    @Test
    void testUpdateCondition() {
        Account account = Account.builder()
                .age(200)
                .gender(1)
                .build();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("user_name", "张三");

        int rows = accountMapper.updateByMap(account, hashMap);
        Assertions.assertEquals(1, rows);

        rows = accountMapper.updateByCondition(account, ACCOUNT.USER_NAME.eq("张三"));
        Assertions.assertEquals(1, rows);

        rows = accountMapper.updateByQuery(account, QueryWrapper.create()
                .where(ACCOUNT.USER_NAME.eq("张三")));
        Assertions.assertEquals(1, rows);
    }

    /*** 禁止全表更新-执行会抛异常 */
    @Test
    void testFullUpdate() {
        Account account = Account.builder().build();
        account.setAge(100);

        // 全表更新
        Assertions.assertThrows(Exception.class,
                () -> accountMapper.updateByQuery(account, QueryWrapper.create()));
    }

    /*** 依据实体部分属性更新-方式1（简单sql更新） */
    @Test
    void testUpdateEntity() {
        Account account = UpdateEntity.of(Account.class, 1);
        account.setId(1L);
        account.setBirthday(new Date());
        account.setAge(18);
        // gender -> null
        // user_name -> null
        // set age = 18
        // set birthday = null, age = 18, gender = null, user_name = null

        int rows = accountMapper.update(account);
        Assertions.assertEquals(1, rows);
    }

    /*** 依据实体部分属性更新-方式2（复杂sql更新） */
    @Test
    void testUpdateWrapper() {
        Account account = UpdateEntity.of(Account.class, 1);
        UpdateWrapper updateWrapper = (UpdateWrapper) account;
        // update set user_name = 'zhangsan', age = age + 1

        updateWrapper.set(ACCOUNT.USER_NAME, "zhangsan");
        updateWrapper.set(ACCOUNT.AGE, ACCOUNT.AGE.add(1));

        int rows = accountMapper.update(account);
        Assertions.assertEquals(1, rows);
    }

    /*** 简单查询-单个数据 */
    @Test
    void testSelectOne() {
        Account account;

        // 根据主键查询
        account = accountMapper.selectOneById(1);
        System.out.println(account);

        // 自定义条件查询-方式1（map）
        Map<String, Object> query = new HashMap<>();
        query.put("id", 2);
        account = accountMapper.selectOneByMap(query);
        System.out.println(account);

        // 自定义条件查询-方式2（依据APT技术生成的类）
        account = accountMapper.selectOneByCondition(ACCOUNT.ID.eq(1));
        System.out.println(account);

        // 自定义条件查询-QueryWrapper
        account = accountMapper.selectOneByQuery(
                QueryWrapper.create()
                        .where(ACCOUNT.ID.eq(2))
        );
        System.out.println(account);
    }

    /*** 简单查询列表(指定列名) */
    @Test
    void testSelectList() {
        List<Account> accounts = accountMapper.selectListByQuery(
                QueryWrapper.create()
                        .select(ACCOUNT.USER_NAME, ACCOUNT.AGE)
                        .where(ACCOUNT.AGE.ge(18))
        );
        accounts.forEach(System.out::println);
    }

    /*** 查询单指定列表 */
    @Test
    void testSelectObject() {
        // 查询单列中的一个
        Object object = accountMapper.selectObjectByQuery(
                QueryWrapper.create()
                        .select(ACCOUNT.AGE, ACCOUNT.USER_NAME)
                        .where(ACCOUNT.ID.eq(1))
        );
        System.out.println(object);
        System.out.println("===========分隔符=========");

        // 查询满足条件的单列列表
        List<Object> objects = accountMapper.selectObjectListByQuery(
                QueryWrapper.create()
                        .select(ACCOUNT.USER_NAME)
                        .where(ACCOUNT.AGE.ge(18))
        );
        System.out.println(objects);
    }

    /*** 查询满足条件的数据行记录 */
    @Test
    void testSelectMap() {
        List<Row> rows = accountMapper.selectRowsByQuery(
                QueryWrapper.create()
                        .where(ACCOUNT.ID.eq(1))
        );
        Row row = rows.get(0);
        System.out.println(row);
        System.out.println(row.getInt("id"));
    }

    /*** 映射查询 */
    @Test
    void testSelectAs() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ALL_COLUMNS,
                        if_(ACCOUNT.AGE.ge(18), true_(), false_()).as("is_adult"))
                .where(ACCOUNT.ID.eq(1));
        // SELECT *, IF(`age` >= ?, TRUE, FALSE) AS `is_adult` FROM `tb_account` WHERE `id` = ?
        AccountVO accountVO = accountMapper.selectOneByQueryAs(queryWrapper, AccountVO.class);
        System.out.println(accountVO);

        queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ALL_COLUMNS,
                        if_(ACCOUNT.AGE.ge(18), true_(), false_()).as("is_adult"))
                .where(ACCOUNT.ID.ge(1));
        // SELECT *, IF(`age` >= ?, TRUE, FALSE) AS `is_adult` FROM `tb_account` WHERE `id` >= ?
        List<AccountVO> accountVOS = accountMapper.selectListByQueryAs(queryWrapper, AccountVO.class);
        accountVOS.forEach(System.out::println);

        queryWrapper = QueryWrapper.create()
                .select(if_(ACCOUNT.AGE.ge(18), true_(), false_()).as("is_adult"))
                .where(ACCOUNT.ID.eq(1));
        // SELECT IF(`age` >= ?, TRUE, FALSE) AS `is_adult` FROM `tb_account` WHERE `id` >= ?
        Integer integer = accountMapper.selectObjectByQueryAs(queryWrapper, Integer.class);
        System.out.println(integer);

        queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.USER_NAME)
                .where(ACCOUNT.ID.ge(1));
        // SELECT `user_name` FROM `tb_account` WHERE `id` >= ?
        List<String> strings = accountMapper.selectObjectListByQueryAs(queryWrapper, String.class);
        System.out.println(strings);
    }

}