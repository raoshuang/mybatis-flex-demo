package com.example.demo.mapper;

import com.example.demo.entity.Card;
import com.mybatisflex.core.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.example.demo.entity.table.CardTableDef.CARD;

/**
 * 信用卡信息持久层测试类
 *
 * @author raos
 * @date 2023-07-06 20:48
 */
@SpringBootTest
public class CardMapperTest {

    @Autowired
    private CardMapper cardMapper;

    /** 测试插入 */
    @Test
    void testInsert() {
        Card card = new Card();
        // idNumber -> id_number
        // 测试监听器生效
        //card.setIdNumber(UUID.randomUUID().toString());
        //card.setCreateTime(LocalDateTime.now());
        card.setTenantId(3);

        cardMapper.insertSelective(card);
        System.out.println(card.getId());
    }

    /*** 测试修改 */
    @Test
    void testUpdate() {
        Card card = new Card();
        card.setId(1L);
        //测试监听器生效
        //card.setUpdateTime(LocalDateTime.now());

        cardMapper.update(card);
    }

    /*** 简单查询 */
    @Test
    void testSelect() {
        cardMapper.selectAll().forEach(System.out::println);
    }

    /*** 简单删除 */
    @Test
    void testDelete() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .where(CARD.ID.eq(5));
        cardMapper.deleteByQuery(queryWrapper);
    }
}