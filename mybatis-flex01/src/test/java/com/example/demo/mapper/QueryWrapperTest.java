package com.example.demo.mapper;

import com.example.demo.entity.Account;
import com.github.vertical_blank.sqlformatter.SqlFormatter;
import com.mybatisflex.core.query.CPI;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryMethods;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.util.StringUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static com.example.demo.entity.extra.table.AccountRoleTableDef.ACCOUNT_ROLE;
import static com.example.demo.entity.table.AccountTableDef.ACCOUNT;
import static com.example.demo.entity.table.RoleTableDef.ROLE;

/**
 * mybatis-flex QueryWrapper查询测试
 *
 * @author raos
 * @date 2023-07-05 23:17
 */
@SpringBootTest
public class QueryWrapperTest {

    private static void printSql(QueryWrapper queryWrapper) {
        String sql = queryWrapper.toSQL();
        System.out.println(SqlFormatter.format(sql));
    }

    /*** QueryWrapper 简单连表查询sql构建 */
    @Test
    void testSimple() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ALL_COLUMNS, ROLE.ALL_COLUMNS)
                .from(ACCOUNT)
                .leftJoin(ACCOUNT_ROLE).on(ACCOUNT_ROLE.ACCOUNT_ID.eq(ACCOUNT.ID))
                .leftJoin(ROLE).on(ACCOUNT_ROLE.ROLE_ID.eq(ROLE.ID))
                .where(ACCOUNT.ID.ge(1))
                .groupBy(ACCOUNT.AGE)
                .having(ACCOUNT.GENDER.in(0, 1))
                .orderBy(ACCOUNT.ID.asc())
                .union(QueryWrapper.create().from(ACCOUNT, ROLE))
                .limit(10);
        printSql(queryWrapper);
    }

    /*** QueryWrapper 动态sql构建 */
    @Test
    void testDynamic() {
        // select * from tb_account
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select().from(ACCOUNT);

        long l = System.currentTimeMillis() / 1000;
        String userName = l % 2 == 0 ? "张三" : null; // null ""

        // where user_name = '张三'
        queryWrapper.where(ACCOUNT.USER_NAME.eq(userName));
        queryWrapper.where(ACCOUNT.USER_NAME.eq(userName).when(userName != null));

        printSql(queryWrapper);
        System.out.println("--------------分割线------------");

        String userName2 = l % 2 == 0 ? "李四" : null;
        boolean flag = StringUtil.isNotBlank(userName2) && !userName2.trim().isEmpty();

        queryWrapper.where(flag ? ACCOUNT.USER_NAME.eq(userName2) : QueryMethods.noCondition());
        printSql(queryWrapper);
        System.out.println("--------------分割线------------");

        queryWrapper.where(ACCOUNT.USER_NAME.eq(userName2).when(flag));
        printSql(queryWrapper);
        System.out.println("--------------分割线------------");

        /*// mybatis-flex 1.5.9 适用
        queryWrapper.where(ACCOUNT.USER_NAME.eq(userName2).when(If::hasText));
        printSql(queryWrapper);
        System.out.println("--------------分割线------------");*/

        queryWrapper.where(ACCOUNT.USER_NAME.eq(userName2, If::hasText));
        printSql(queryWrapper);
    }

    /*** QueryWrapper sql函数查询语句 */
    @Test
    void testFunction() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(QueryMethods.concat(ACCOUNT.USER_NAME, QueryMethods.string("123")))
                .from(ACCOUNT)
                .where(QueryMethods.not(ACCOUNT.GENDER.eq(3)));
        printSql(queryWrapper);
    }

    /*** QueryWrapper 自定义列原始方式 */
    @Test
    void testRaw() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ID, ACCOUNT.USER_NAME, ACCOUNT.AGE)
                .from(ACCOUNT)
                .where(ACCOUNT.ID.eq(1));
        printSql(queryWrapper);
    }

    /*** QueryWrapper 自定义列Lambda式方式 */
    @Test
    void testLambda() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(Account::getId, Account::getUserName, Account::getAge)
                .from(Account.class)
                .where(Account::getId).eq(1);
        printSql(queryWrapper);
    }

    /*** QueryWrapper 字符串定义列查询 */
    @Test
    void testString() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select("id", "user_name", "age")
                .from("tb_account")
                .where("id = ?", 1);
        printSql(queryWrapper);
    }

    /*** QueryWrapper CPI操作 */
    @Test
    void testCPI() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ID, ACCOUNT.USER_NAME, ACCOUNT.AGE)
                .from(ACCOUNT)
                .where(ACCOUNT.ID.ge(3))
                .and(ACCOUNT.AGE.ge(18))
                .limit(10);

        CPI.getSelectColumns(queryWrapper).forEach(System.out::println);
        System.out.println("--------------分割线------------");
        printSql(queryWrapper);

        System.out.println("--------------分割线------------");
        CPI.setLimitRows(queryWrapper, 1L);
        printSql(queryWrapper);
    }

    /*** QueryWrapper 查询Wrapper克隆 */
    @Test
    void testClone() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(ACCOUNT.ID, ACCOUNT.USER_NAME, ACCOUNT.AGE)
                .from(ACCOUNT)
                .where(ACCOUNT.ID.eq(1));

        QueryWrapper clone = queryWrapper.clone();

        CPI.setSelectColumns(clone, null);

        printSql(queryWrapper);
        System.out.println("--------------分割线------------");
        printSql(clone);
    }

}
