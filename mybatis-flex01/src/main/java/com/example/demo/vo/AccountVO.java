package com.example.demo.vo;

import lombok.Data;

import java.util.Date;

/**
 * 账户展示类
 *
 * @author rso
 * @since 2023-07-05 22:37
 */
@Data
public class AccountVO {

    private Long id;
    private String userName;
    private Integer age;
    private Integer gender;
    private Date birthday;
    private Boolean isAdult;

}