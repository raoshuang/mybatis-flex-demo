package com.example.demo.mapper;

import com.example.demo.entity.Account;
import com.mybatisflex.core.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 账号持久层
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 20:58
 */
@Repository
public interface AccountMapper extends BaseMapper<Account> {

}
