package com.example.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * mybatis-flex 学习项目启动类
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 20:55
 **/
@MapperScan("com.example.demo.mapper")
@SpringBootApplication
public class MybatisFlex01App {

    public static void main(String[] args) {
        SpringApplication.run(MybatisFlex01App.class, args);
    }

}
