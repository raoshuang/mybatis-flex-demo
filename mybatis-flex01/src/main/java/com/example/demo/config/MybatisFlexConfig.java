package com.example.demo.config;

import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.mybatis.FlexConfiguration;
import com.mybatisflex.spring.boot.ConfigurationCustomizer;
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer;
import com.mybatisflex.spring.boot.SqlSessionFactoryBeanCustomizer;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisFlex 配置类
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 21:35
 */
@Configuration
public class MybatisFlexConfig implements MyBatisFlexCustomizer,
        ConfigurationCustomizer,
        SqlSessionFactoryBeanCustomizer {

    /** 配置加载顺序 MyBatis Flex -> MyBatis Configuration -> SqlSessionFactoryBean */

    @Override
    public void customize(FlexGlobalConfig globalConfig) {
        System.out.println("MyBatis Flex 配置！");
    }

    @Override
    public void customize(FlexConfiguration configuration) {
        configuration.setLogImpl(StdOutImpl.class);
        System.out.println("MyBatis Configuration 配置！");
    }

    @Override
    public void customize(SqlSessionFactoryBean sqlSessionFactoryBean) {
        System.out.println("SqlSessionFactoryBean 配置！");
    }

}
