package com.example.demo.listener;

import com.example.demo.entity.Card;
import com.mybatisflex.annotation.AbstractUpdateListener;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * 信用卡持久化处理修改监听器
 *
 * @author raos
 * @date 2023-07-06 20:58
 */
@Slf4j
public class CardUpdateListener extends AbstractUpdateListener<Card> {

    @Override
    public Class<Card> supportType() {
        return Card.class;
    }

    @Override
    public void doUpdate(Card card) {
        log.info("执行 CardUpdateListener 中的 doUpdate 方法。");
        card.setUpdateTime(LocalDateTime.now());
    }

    @Override
    public int order() {
        return 1;
    }

}