package com.example.demo.listener;

import com.example.demo.entity.Card;
import com.mybatisflex.annotation.AbstractInsertListener;
import com.mybatisflex.core.util.StringUtil;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 信用卡持久化处理插入监听器
 *
 * @author raos
 * @date 2023-07-06 20:58
 */
@Slf4j
public class CardInsertListener extends AbstractInsertListener<Card> {

    @Override
    public Class<Card> supportType() {
        return Card.class;
    }

    @Override
    public void doInsert(Card card) {
        log.info("执行 CardInsertListener 中的 doInsert 方法");
        if (StringUtil.isBlank(card.getIdNumber())) {
            card.setIdNumber(UUID.randomUUID().toString());
        }
        card.setCreateTime(LocalDateTime.now());
    }

    @Override
    public int order() {
        return 1;
    }

}