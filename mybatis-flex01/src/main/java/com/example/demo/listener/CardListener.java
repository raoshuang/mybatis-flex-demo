package com.example.demo.listener;

import com.example.demo.entity.Card;
import com.mybatisflex.annotation.InsertListener;
import com.mybatisflex.annotation.UpdateListener;
import com.mybatisflex.core.util.StringUtil;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 信用卡持久化处理监听器
 *
 * @author raos
 * @date 2023-07-06 20:55
 */
public class CardListener implements InsertListener, UpdateListener {

    @Override
    public void onInsert(Object entity) {
        Card card = (Card) entity;
        if (StringUtil.isBlank(card.getIdNumber())) {
            card.setIdNumber(UUID.randomUUID().toString());
        }
        card.setCreateTime(LocalDateTime.now());
    }

    @Override
    public void onUpdate(Object entity) {
        Card card = (Card) entity;
        card.setUpdateTime(LocalDateTime.now());
    }

}