package com.example.demo.listener;

import com.mybatisflex.annotation.InsertListener;
import com.mybatisflex.annotation.UpdateListener;
import lombok.extern.slf4j.Slf4j;

/**
 * 信用卡持久化处理日志打印监听器
 *
 * @author raos
 * @date 2023-07-06 20:58
 */
@Slf4j
public class LoggerListener implements InsertListener, UpdateListener {

    @Override
    public void onInsert(Object entity) {
        log.info("插入数据：{}", entity);
    }

    @Override
    public void onUpdate(Object entity) {
        log.info("更新数据：{}", entity);
    }

}