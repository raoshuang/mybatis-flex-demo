package com.example.demo.listener;

import com.example.demo.entity.Account;
import com.mybatisflex.annotation.SetListener;

/**
 * 账户持久化处理onSet之字段字典回显监听器
 *
 * @author raos
 * @date 2023-07-06 21:06
 */
public class AccountDictListener implements SetListener {

    @Override
    public int order() {
        return 1;
    }

    @Override
    public Object onSet(Object entity, String property, Object value) {
        if (isGender(property)) {
            Account account = (Account) entity;
            // noinspection AlibabaSwitchStatement
            switch ((Integer) value) {
                case 1 -> account.setGenderName("女");
                case 2 -> account.setGenderName("男");
                default -> account.setGenderName("未知");
            }
        }

        // 别忘记返回其他属性的值！！！
        return value;
    }

    private boolean isGender(String property) {
        return "gender".equals(property);
    }

}