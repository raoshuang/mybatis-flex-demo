package com.example.demo.entity;

import com.example.demo.listener.AccountDictListener;
import com.example.demo.listener.AccountEncryptListener;
import com.example.demo.listener.AccountPermissionListener;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.*;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * 账号实体类
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2023-07-05 20:57
 */
// 覆盖toString方法不生效时参考 https://blog.csdn.net/pingzhuyan/article/details/130867113 解决
@Builder
@ToString
@Data
@Table(value = "tb_account", mapperGenerateEnable = false,
        onSet = {AccountPermissionListener.class, AccountEncryptListener.class, AccountDictListener.class}
)
public class Account implements Serializable {

    @Id(keyType = KeyType.Auto)
    private Long id;
    private String userName;
    private String password;
    private Integer gender;
    private Integer age;
    private Date birthday;
    @Column(ignore = true)
    private String genderName; // 性别翻译字段

    /*** 无参构造器-使用@Tolerate兼容@Builder导致无参构造器私有化无法被反射使用的问题 */
    @Tolerate
    public Account() { }

}
