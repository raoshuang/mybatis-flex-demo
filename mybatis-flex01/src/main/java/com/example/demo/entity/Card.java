package com.example.demo.entity;

import com.example.demo.listener.CardInsertListener;
import com.example.demo.listener.CardListener;
import com.example.demo.listener.CardUpdateListener;
import com.example.demo.listener.LoggerListener;
import com.mybatisflex.annotation.*;
import com.mybatisflex.core.handler.Fastjson2TypeHandler;
import com.mybatisflex.core.mask.Masks;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 信用卡信息
 *
 * @author raos
 * @date 2023-07-06 20:42
 */
@Data
@Table(value = "tb_card", camelToUnderline = false,
        /*onInsert = CardListener.class, // 插入监听器
        onUpdate = CardListener.class // 更新监听器*/
        onInsert = {CardInsertListener.class, LoggerListener.class},
        onUpdate = {CardUpdateListener.class, LoggerListener.class}
)
public class Card {

    @Id(keyType = KeyType.Auto)
    private Long id;

    private String idNumber;

    private String location;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer tenantId;

}
