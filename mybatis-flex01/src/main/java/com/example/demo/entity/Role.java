package com.example.demo.entity;

import com.mybatisflex.annotation.*;
import lombok.Data;

/**
 * 角色实体类
 *
 * @author raos
 * @date 2023-07-05 23:21
 */
@Data
@Table("tb_role")
public class Role {

    @ColumnAlias("role_id")
    @Id(keyType = KeyType.Auto)
    private Long id;
    private String roleKey;
    private String roleName;
    @Column(isLarge = true)
    private String description;

}