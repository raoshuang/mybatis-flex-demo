package com.example.demo.entity.extra;

import com.mybatisflex.annotation.Table;
import lombok.Data;

/**
 * 账号角色实体类
 *
 * @author raos
 * @date 2023-07-05 23:21
 */
@Data
@Table(value = "tb_account_role", mapperGenerateEnable = false)
public class AccountRole {
    private Long accountId;
    private Long roleId;

}
