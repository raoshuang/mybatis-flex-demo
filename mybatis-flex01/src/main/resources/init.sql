-- init

-- 创建账户表
DROP TABLE IF EXISTS `tb_account`;
CREATE TABLE `tb_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO tb_account(id, user_name, age, birthday)
VALUES (1, '张三', 18, '2020-01-11'),(2, '李四', 19, '2021-03-21');

-- 添加性别字段
ALTER TABLE `tb_account` ADD COLUMN `gender` tinyint(1) NULL DEFAULT 3 AFTER `user_name`;

-- 角色表
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
   `role_key` varchar(255) DEFAULT NULL COMMENT '角色key',
   `role_name` varchar(255) DEFAULT NULL COMMENT '角色名',
   `description` varchar(255) DEFAULT NULL COMMENT '角色说明',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_role` VALUES (1, 'admin', '管理员用户', NULL);
INSERT INTO `tb_role` VALUES (2, 'svip', '超级贵族用户', NULL);
INSERT INTO `tb_role` VALUES (3, 'vip', '贵族用户', NULL);
INSERT INTO `tb_role` VALUES (4, 'common', '普通用户', NULL);

-- 账户角色表
DROP TABLE IF EXISTS `tb_account_role`;
CREATE TABLE `tb_account_role` (
   `account_id` int(11) NOT NULL COMMENT '账号ID',
   `role_id` int(11) NOT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_account_role` VALUES (1, 1);
INSERT INTO `tb_account_role` VALUES (1, 2);
INSERT INTO `tb_account_role` VALUES (2, 3);
INSERT INTO `tb_account_role` VALUES (2, 4);

-- 信用卡表
DROP TABLE IF EXISTS `tb_card`;
CREATE TABLE `tb_card` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `idNumber` varchar(255) DEFAULT NULL COMMENT 'ID编号',
   `location` varchar(255) DEFAULT NULL COMMENT '位置',
   `createTime` datetime DEFAULT NULL COMMENT '创建时间',
   `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
   `delFlag` tinyint(1) DEFAULT '0' COMMENT '删除标记(0-存活/1-删除)',
   `version` int(11) DEFAULT '0' COMMENT '版本',
   `tenantId` int(11) DEFAULT '0' COMMENT '租户ID',
   `options` varchar(255) DEFAULT NULL COMMENT '选项信息',
   `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
   `type` varchar(255) DEFAULT NULL COMMENT '类型',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用卡';

INSERT INTO `tb_card`
VALUES (1, '22399af8-cb96-44df-9897-abd51df20e1d', 'CN', '2023-07-06 21:08:43', '2023-07-30 15:19:12', 0, 1, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (2, '135adc58-3ed5-4e85-aae4-c2d49a6b4ac9', NULL, '2023-07-06 21:31:22', NULL, 0, 0, 1, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (3, '09292b2e-d6c9-424e-8e53-2c14d4803a17', NULL, '2023-07-06 21:36:39', NULL, 0, 0, 1, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (4, '31958a00-dbba-4112-ba6a-ecb1a69e82d0', NULL, '2023-07-06 21:41:13', NULL, 0, 0, 2, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (5, '971406f1-a550-4f88-b471-b9c68e60695e', NULL, '2023-07-06 21:21:43', NULL, 1, 0, 2, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (6, '5ee1b37a-2a5b-45e4-8aff-f7adedc331ee', NULL, '2023-07-06 21:28:00', NULL, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (7, '6c2b0307-9e9e-4e66-af20-a9128864053d', NULL, '2023-07-06 21:35:11', NULL, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (9, '581bd0a3-9626-4691-85d2-99a56c552c87', NULL, '2023-07-06 21:31:55', NULL, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (11, '88ad2dbc-d2ce-4a73-bad9-865c6e30977e', NULL, '2023-07-06 21:33:55', NULL, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (12, '6f870312-71a5-4389-b789-dd5f0296ae8e', NULL, '2023-07-06 21:55:31', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (13, '28af54cb-1d94-4a56-bb56-df006a02656a', NULL, '2023-07-06 21:56:51', NULL, 0, 0, 2, NULL, NULL, NULL);
INSERT INTO `tb_card`
VALUES (14, '227304ae-84a5-4d01-baf4-99808f733195', NULL, '2023-07-06 21:20:48', NULL, 0, 0, 0, '{
  \"age\": 100,
  \"name\": \"x100\"
}', '1234567890@qq.com', NULL);
INSERT INTO `tb_card`
VALUES (20, 'e37a1fa1-3be5-4460-84de-213ddfd600c8', NULL, '2023-07-06 21:15:49', NULL, 0, 0, 0, NULL, NULL, 'A');
INSERT INTO `tb_card`
VALUES (21, '961fff82-a7bc-462f-91a3-36d4ec576f8c', NULL, '2023-07-06 21:17:12', NULL, 0, 0, 0, NULL, NULL, 'AA');
INSERT INTO `tb_card`
VALUES (23, '28000', NULL, '2023-08-11 17:21:39', NULL, 0, 0, 0, NULL, NULL, NULL);

-- 账户表添加密码字段
ALTER TABLE `tb_account` ADD COLUMN `password` varchar(255) NULL DEFAULT '123456' AFTER `user_name`;

